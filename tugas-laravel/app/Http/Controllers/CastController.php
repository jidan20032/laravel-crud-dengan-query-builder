<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Expr\Cast;


class CastController extends Controller
{
    public function index()
    {
        $cast = DB::table("cast")->get();
        return view("cast.cast", [
            "title" => "Cast",
            "name" => "Cast",
            "cast" => $cast
        ]);
    }

    public function create()
    {
        return view("cast.create", [
            "title" => "Create Cast",
            "name" => "Create Cast"
        ]);
    }

    public function store(Request $request)
    {
        $validation = $request->validate([
            "nama" => "required",
            "umur" => "required",
            "bio" => "required",
        ]);

        $query = DB::table('cast')->insert([
            'nama' => $request->nama,
            'umur' => $request->umur,
            'bio' => $request->bio,
            'created_at'=> now()
        ]);
        return redirect('/cast')->with('success', 'Data Berhasil Ditambahkan');
    }
    public function show($cast_id)
    {
        $cast = DB::table('cast')->find($cast_id);
        return view('cast.show', [
            "title" => "Cast",
            "name" => "Detail Cast",
            "cast" => $cast
        ]);
    }

    public function edit($cast_id)
    {
        $cast = DB::table('cast')->find($cast_id);
        return view('cast.edit', [
            "title" => "Cast",
            "name" => "Edit Cast",
            "cast" => $cast
        ]);
    }

    public function update(Request $request, $cast_id)
    {
        $data = $request->validate([
            "nama"=> "required",
            "umur"=> "required",
            "bio"=> "required"
            ]);
        $data["updated_at"] = now();
        $query = DB::table("cast")->where ("id", $cast_id)->update($data);
        return redirect("/cast")->with("success","Data Berhasil Di Ubah");
    }

    public function destroy($cast_id)
    {
        $cast = DB::table('cast')->find( $cast_id);
        $query = DB::table('cast')->delete([
           'cast'=> $cast_id
        ]);
        return redirect('/cast')->with('success', 'Data Berhasil Di Hapus');
    }
}
