<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DataTableController extends Controller
{
    public function DataTable()
    {
        return view("data-table", [
            "title" => "Data Table",
            "name" => "Data Table",
        ]);
    }
}
