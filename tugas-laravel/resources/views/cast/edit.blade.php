@extends('layouts.master')

@section('content')
<section class="content">
    <div class="container-fluid">
          <div class="card card-light">
            <div class="card-header">
              <h3 class="card-title">Edit Cast</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="/cast/{{ $cast->id }}" method="POST">
                @method('put')
                @csrf
              <div class="card-body">
                <div class="form-group">
                  <label for="nama">Nama</label>
                  <input type="text" class="form-control" id="name" name="nama" placeholder="Masukkan Nama" required autofocus value="{{ old('nama', $cast->nama) }}">
                </div>
                @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror

                <div class="form-group">
                  <label for="umur">Umur</label>
                  <input type="number" class="form-control" id="umur" name="umur" placeholder="Masukkan Umur" required autofocus value="{{ old('umur', $cast->umur) }}">
                </div>
                @error('umur')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror

                <div class="form-group">
                    <label for="bio">Bio</label>
                    <input type="text" class="form-control" id="bio" name="bio" placeholder="Masukkan Bio" required autofocus value="{{ old('bio', $cast->bio) }}">
                  </div>
                  @error('bio')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
              </div>
                
              <!-- /.card-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-success float-right">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.card -->

@endsection