@extends('layouts.master')

@section('content')
<section>
    <div class="class content-wrepper">
        <div class=" float-sm-right">
            <div class="new ">
                <a href="/cast/create" class="btn btn-primary mb-3 ">Add New +</a>
            </div>
        </div>
    </div>
   
    <div class="body">
        @if (session('success'))
        <div class="alert alert-info">{{ session('success') }}</div>            
        @endif
    </div>
    <table id="cast" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th style="text-align:center; font-weight:bold; width:5%">No</th>
                <th style="text-align:center; font-weight:bold; width:20%">Nama</th>
                <th style="text-align:center; font-weight:bold; width:5%">Umur</th>
                <th style="text-align:center; font-weight:bold; width:40%">Bio</th>
                <th style="text-align:center; font-weight:bold; width:15%">Aksi</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key => $value)
            <tr>
                <td style="text-align: center">{{ $key+1 }}</td>
                <td>{{ $value->nama }}</td>
                <td style="text-align: center">{{ $value->umur }}</td>
                <td>{{ $value->bio }}</td>
                <td style="text-align: center">
                    <div class="new">
                        <a href="/cast/{{ $value->id }}" class="btn btn-primary"><i class="fas fa-eye"></i></a>
                        
                        <a href="/cast/{{ $value->id }}/edit" class="btn btn-primary"><i class="fas fa-pen"></i></a>
                        
                        <form action="/cast/{{ $value->id }}" method="POST" class="d-inline">
                        @method('delete')
                        @csrf
                        <button class="btn btn-primary" onclick="return confirm('Are You Sure?')"><i class="fas fa-trash"></i></button>    
                    </form>
                       
                    </div>

                    
                </td>
            </tr>
            @empty
            <tr>
                <td colspan="4" align="center">No Data</td>
            </tr>
            @endforelse
        </tbody>
    </table>
</section>
@endsection
