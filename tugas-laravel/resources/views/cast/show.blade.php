@extends('layouts.master')

@section('content')
<section class="content">

    <!-- Default box -->
    <div class="card">
      <div class="card-header">
        <h1>Detail Cast</h1>
       
            <ul>
                <li><strong>Nama :</strong> {{ $cast->nama }}</li>
                <li><strong>Umur :</strong> {{ $cast->umur }}</li>
                <li><strong>Bio :</strong> {{ $cast->bio }}</li>
                <li><strong>Created_at :</strong> {{ $cast->created_at }}</li>
                <li><strong>Update_at :</strong> {{ $cast->updated_at }}</li>
            </ul>
       
        <a href="{{ url('/cast') }}" class="btn btn-primary">Kembali</a>
      </div>
    </section>
@endsection
