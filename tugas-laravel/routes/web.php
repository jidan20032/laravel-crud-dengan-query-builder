<?php

use App\Http\Controllers\DashboardController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TableController;
use App\Http\Controllers\DataTableController;
use App\Http\Controllers\CastController;
use PhpParser\Node\Expr\Cast;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/welcome', function () {
    return view('welcome');
});

//table
route::get('/', [DashboardController::class, 'dashboard']);
route::get('/table', [TableController::class, 'table']);
route::get('/datatable', [DataTableController::class, 'DataTable']);

//cast 
//create
route::get('/cast', [CastController::class, 'index']);
route::get('/cast/create', [CastController::class, 'create']);
route::post('/cast', [CastController::class, 'store']);
route::get('/cast/{cast_id}', [CastController::class, 'show']);

// //edit
route::get('/cast/{cast_id}/edit', [CastController::class,'edit']);
route::put('/cast/{cast_id}', [CastController::class,'update']);

// delete
route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);
